#!/bin/sh
set -e

export TF_VAR_account_file_path=$(stat -c %n ~/.gcloud/account.json)
export TF_VAR_ca_shared_key=$(cat ca-shared-key.txt)
export TF_VAR_commit=$(git rev-parse HEAD | cut -c 1-16)
export TF_VAR_project=$(gcloud config get-value project)
export TF_VAR_region=$(gcloud config configurations describe default --format json | jq .properties.compute.region | tr --delete '"')
export TF_VAR_ssh_private_key_path=$(stat -c %n ~/.ssh/id_ed25519)
export TF_VAR_ssh_user=$(gcloud compute os-login describe-profile --format json | jq .posixAccounts[].username | tr --delete '"')
export TF_VAR_ENVIRONMENT=production
export TF_VAR_ca_organization_name=$(gcloud compute project-info describe --format json | jq '.commonInstanceMetadata.items[] | select(.key == "organization").value' | tr -d '"')

terraform init
terraform plan
