#!/bin/sh
set -e

ACCOUNT_FILE=$(stat -c %n ~/.gcloud/account.json)
PROJECT_ID=$(gcloud config get-value project)
COMMIT=$(git rev-parse HEAD)
BRANCH=$(git branch | grep \* | cut -d ' ' -f2)
OS_LOGIN_USER=$(gcloud compute os-login describe-profile --format json | jq .posixAccounts[].username | tr --delete '"')
SSH_PRIVATE_KEY_PATH=$(stat -c %n ~/.ssh/id_ed25519)

find . -maxdepth 1 -name "*.json" -exec \
    packer validate \
    -var ssh_username=${OS_LOGIN_USER} \
    -var ssh_private_key_file=${SSH_PRIVATE_KEY_PATH} \
    -var account_file=${ACCOUNT_FILE} \
    -var project_id=${PROJECT_ID} \
    -var environment=${ENVIRONMENT:-production} \
    -var branch=${BRANCH} \
    -var commit=${COMMIT} \
    {} \;

find . -maxdepth 1 -name "*.json" -exec \
    packer build \
    -var ssh_username=${OS_LOGIN_USER} \
    -var ssh_private_key_file=${SSH_PRIVATE_KEY_PATH} \
    -var account_file=${ACCOUNT_FILE} \
    -var project_id=${PROJECT_ID} \
    -var environment=${ENVIRONMENT:-production} \
    -var branch=${BRANCH} \
    -var commit=${COMMIT} \
    {} \;
