#!/bin/bash

set -e

sudo mkdir -p /etc/cfssl
sudo chown --recursive cfssl:cfssl /etc/cfssl
