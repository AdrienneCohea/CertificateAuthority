
data "template_file" "cfssl_authority_config" {
  template = "${file("cfssl-authority-config.json")}"

  vars {
    ca_shared_key = "${var.ca_shared_key}"
  }
}
