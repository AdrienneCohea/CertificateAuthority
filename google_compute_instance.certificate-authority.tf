resource "google_compute_instance" "certificate-authority" {
  name         = "certificate-authority"
  machine_type = "g1-small"
  zone         = "us-west1-a"
  tags         = ["certificate-authority", "${var.environment}"]

  boot_disk {
    initialize_params {
      image = "${data.google_compute_image.certificate_authority.self_link}"
    }
  }

  network_interface {
    network = "default"
    access_config { }
  }

  can_ip_forward = true

  service_account {
    scopes = ["compute-rw", "storage-ro", "service-management", "service-control", "logging-write", "monitoring"]
  }

  connection { user = "${var.ssh_user}" private_key = "${file(var.ssh_private_key_path)}" }

  provisioner "file" { source = "files/tls/root.pem" destination = "/tmp/root.pem" }
  provisioner "file" { source = "files/tls/intermediate-key.pem" destination = "/tmp/intermediate-key.pem" }
  provisioner "file" { source = "files/tls/intermediate.pem" destination = "/tmp/intermediate.pem" }
  provisioner "file" { content = "${data.template_file.ca_server_csr.rendered}" destination = "/tmp/ca-server-csr.json" }
  provisioner "file" { content = "${data.template_file.cfssl_authority_config.rendered}" destination = "/tmp/cfssl-authority-config.json" }

  provisioner "remote-exec" {
    "inline" = [
      "sudo chown cfssl:cfssl /tmp/intermediate*pem /tmp/cfssl-authority-config.json ca-server-csr.json",
      "sudo mv /tmp/root.pem /usr/local/share/ca-certificates/root.crt",
      "sudo mv /tmp/intermediate-key.pem /etc/cfssl/intermediate-key.pem",
      "sudo mv /tmp/intermediate.pem /etc/cfssl/intermediate.pem",
      "sudo mv /tmp/cfssl-authority-config.json /etc/cfssl/config.json",
      "sudo mv /tmp/ca-server-csr.json /etc/cfssl/ca-server-csr.json"
    ]
  }
}
