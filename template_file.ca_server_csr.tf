data "template_file" "ca_server_csr" {
  template = "${file("ca-server-csr.json")}"

  vars {
    project      = "${var.project}"
    organization = "${var.ca_organization_name}"
  }
}
