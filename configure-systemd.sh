#!/bin/bash
set -e

sudo mv /tmp/cfssl.path /lib/systemd/system/cfssl.path
sudo mv /tmp/cfssl.service /lib/systemd/system/cfssl.service
sudo systemctl daemon-reload
sudo systemctl enable cfssl.path
sync
