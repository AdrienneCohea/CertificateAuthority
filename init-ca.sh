#!/bin/bash
set -e

cd files/tls

[ -f root.pem ] || cfssl genkey -initca root.json | cfssljson -bare root
cfssl genkey -initca intermediate.json | cfssljson -bare intermediate
cfssl sign -ca root.pem -ca-key root-key.pem -config config.json intermediate.csr | cfssljson -bare intermediate
